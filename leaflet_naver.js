(function($) {
   	Drupal.behaviors.leafletnaver = {
    	attach: function (context) {

    	var Overlayset = 0;
        var $window = $(window);
      	$(window).resize(leafletmod);
      	leafletmod();

    	function leafletmod(){
                if(Overlayset==0){
                  $('<div id="leafoverley"><div class="naver">Naviger i kart</div></div>').insertBefore('#leaflet-map');
                  Overlayset = 1;
                  }
                  $('#leafoverley').parent().css('position', 'relative');
                  mapheight = $('#leaflet-map').outerHeight(true);
                  mapwidth = $('#leaflet-map').outerWidth(true);
                  $('#leafoverley').height(mapheight);
                  $('#leafoverley').width(mapwidth);
                  $('#leafoverley').css('position', 'absolute');
                  $('#leafoverley').css('right', '0');
                  $('#leafoverley').css('z-index', '1000');
                  $('.leaflet-top, .leaflet-bottom').css('z-index', '-1');

              }
              Naverclick = 0;
              naver = $('.naver')
              $(naver).click(function(){

                  if(Naverclick==0){
                    Naverclick = 1;
                    $('#leafoverley').css('height', '30px');
                    $('#leafoverley').css('width', '90px');
                    $('.leaflet-top, .leaflet-bottom').css('z-index', '1000');
                    $('.naver').css('background', '#c6093b');
                    $('.naver').css('color', '#ffffff');
                  }else{
                    Naverclick = 0;
                    $('#leafoverley').height(mapheight);
                    $('#leafoverley').width(mapwidth);
                    $('.leaflet-top, .leaflet-bottom').css('z-index', '-1');
                    $('.naver').css('background', '#ffffff');
                    $('.naver').css('color', '#000000');                 
                  }
                  
                });
	}
  };
})(jQuery);